
# pYTerm 0.5

pYTerm is a simple command line/python lib youtube music player using python 3. This is still currently under heavy development and
features are subject to being changed and things being added. Bugs *will* happen.
---

## Requirements

* All libraries from `requirements.txt` if using the source code instead of the pip package.
* Patience

While not required, having VLC Media Player installed is *__highly__* recommended. The new vlc-less audio backend is experimental and unstable, so vlc will be preferred and used automatically if installed. 

---

More details & api documentation over at [the docs](https://pyterm.readthedocs.io/en/latest/index.html)