==================================
Getting started
==================================

Installation
----------------

Before using pYTerm, it is highly recommended you `install vlc media player <https://www.videolan.org/vlc/>`_ 
while this isn't required, and pYTerm will *mostly* work using the alternative FFPyPlayer backend, this is currently quite unstable and can lead to unexpected errors and crashes.

You can install pYTerm through pip with the following command:

.. code-block:: bash

    python -m pip install p-yt-erm

And then import it like this:

.. code-block:: python
    :linenos:

    from pYTerm import pYTerm

Usage
-------------

The most straight-forward way of using pYTerm is by just calling .play() like this:

.. code-block:: python
    :linenos:

    from pYTerm import pYTerm
    
    myplayer = pYTerm.Player()
    myplayer.play("Girl in red - body and mind")

which will just play the song without any hassle.
This is just scratching the surface of pYTerm though! Here's a slightly more involved example:


.. code-block:: python
    :linenos:

    from pYTerm import pYTerm
    
    myplayer = pYTerm.Player( volume=50, playlists = 'the first glass beach album' )

    myplayer.shuffle()

    myplayer.play_all( halting = True, keep_alive = False )
    # will wait until the playlist is finished playing

    myplayer.add_song('girl in red - body and mind')

    myplayer.play_all( halting = False, keep_alive = True )
    # will not wait

    myplayer.add_song('https://www.youtube.com/watch?v=20SbrlLmOJA')

    myplayer.add_playlist('life is strange original soundtrack')

All player controls such as skipping, timeline scrubbing, etc. are also accessible through various class functions:

.. code-block:: python
    :linenos:

    from pYTerm import pYTerm

    myplayer = pYTerm.Player( volume=50, playlists = 'the first glass beach album' )
    myplayer.play_all( halting = False, keep_alive = True )

    myplayer.wait_on_song_load()
    
    myplayer.next()
    myplayer.wait_on_song_load()

    myplayer.pause()
    myplayer.toggle_mute()
    myplayer.unmute()
    myplayer.unpause()
    myplayer.scrub(10) # fast forward 10 seconds
    myplayer.scrub(-5) # fast backwards 5 seconds

    myplayer.next()
    myplayer.wait_on_song_load()

    while myplayer.is_alive():
        print('song is at', myplayer.get_current_time(), 'out of', myplayer.get_total_time())


To see a list of everything you can toy around with, take a look at :ref:`main player class documentation<section-mpc>`.