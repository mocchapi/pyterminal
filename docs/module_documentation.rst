Module documentation
==================================


.. _section-mpc:

Main player class
------------------
The main player providing most functionality you would need

.. automodule:: pYTerm.pYTerm
   :members:

Tools module
----------------
The tools is full of extremely messy code that we banned to its own file so we dont have to look at it.
It provides a bunch of functions to make the main player work more nicely without filling up the .py

.. automodule:: pYTerm.tools
   :members:

backends module
------------------
The backends module provides a standardised way to integrate an audio backend with pYTerm. pYTerm currently has 2 audio backends:

* VLCBackend, highly recommended but only works if your system has `vlc media player <https://www.videolan.org/vlc/>`_  installed
* FFPyPlayerBackend, which works out of the box without any further installation, but is a lot less stable and prone to errors

.. automodule:: pYTerm.backends
   :members:
