==================================
UI projects using pYTerm
==================================

Small list of projects that add an actual user interface, for if you don't feel like using the commandline.

* `pYTermQT <https://gitlab.com/mocchapi/pyterm-qt>`_, using pyQT. Probably the most usable.
* `pYTerm-ursina <https://gitlab.com/mocchapi/pyterm-ursina>`_, using the ursina game engine. Very bad very gimmick.
