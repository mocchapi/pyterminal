==================================
Running from the commandline
==================================

Alongside the python package, pYTerm also has a CLI for playing those same youtube songs/playlists/whatever from your very own terminal/commandline
When you've installed the package, you can simply use `pYTerm` to call it.

Here's the help page:

.. code-block:: bash

    usage: pYTerm [-h] [--version] [-v VOLUME] [-l] [-s] [-p PLAYLISTS] [--nopresence] [--muted] [--noinput] [--legacy] [--verbose] [--vlclogs] ...

    Play youtube audio from the commandline / écouter l'audio des vidéos youtube sur la ligne de commande

    positional arguments:
    songs                 Name or url of the song(s) you want to play / nom de la chanson à jouer tu veux jouer

    optional arguments:
    -h, --help            show this help message and exit
    --version             Prints version / version imprimé
    -v VOLUME, --volume VOLUME
                            Starts with <value> volume / le programme démarrer avec un niveau de volume <value>
    -l, --loop            Enable queue looping
    -s, --shuffle         Enable queue shuffling
    -p PLAYLISTS, --playlist PLAYLISTS
                            Add local or youtube playlist / utiliser une playlist à partir d'un fichier
    --nopresence          Disable discord rich presence
    --muted               Start player muted
    --noinput             Disable player controls / désactiver les contrôles
    --legacy              Forces the use legacy streams, use this if some songs dont load
    --verbose             Enable debug logging
    --vlclogs             Enable vlc logging


Playing a song is straight forward, just call pYTerm with any optional arugments you want to use and put the song(s) you want to listen to at the end like this:

.. code-block:: bash

    pYTerm --volume 50 "girl in red - body and mind"
    