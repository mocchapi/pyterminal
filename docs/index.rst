Welcome to pYTerm's documentation!
==================================

pYTerm is a simple command line/programmatical youtube music player using python 3.
These pages document the various functions and classes of this module, and while most of the main player class *is* documented, there's still a lot that isn't:
This is very much WIP.

If you find any issues or unexpected exceptions, please open an issue on `our gitlab <https://gitlab.com/mocchapi/pyterminal/-/issues>`_

.. toctree::
   :maxdepth: 2

   getting_started
   commandline
   Ui_wrappers
   module_documentation
.. additional_explanation

